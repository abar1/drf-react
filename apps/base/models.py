from django.db import models

class BaseModel (models.Model):

    id = models.AutoField(primary_key=True)
    state = models.BooleanField("Estado", default=True)
    created_on = models.DateTimeField("Fecha creación", auto_now=True)
    modified_on = models.DateTimeField("Fecha modificación", auto_now=True)
    deleted_on = models.DateTimeField("Fecha eliminación", auto_now=True)


    class Meta:

        abstract = True
        verbose_name = "Base model"
        verbose_name_plural = "Bases model"