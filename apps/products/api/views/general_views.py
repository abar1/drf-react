from rest_framework import generics
from apps.base.api import GeneralListApiView
from apps.products.models import MeasureUnit,Indicator, ProductCategory
from apps.products.api.api_serializers.general_serializers import MesureUnitSerializer, IndicatorSerializer, ProductCategorySerializer

class MesaureUnitListAPIView(GeneralListApiView):
    serializer_class = MesureUnitSerializer

    # def get_queryset(self):
    #     return MeasureUnit.objects.filter(state = True)


class IndicatorListAPIView(GeneralListApiView):
    serializer_class = IndicatorSerializer

    # def get_queryset(self):
    #     return Indicator.objects.filter(state = True)


class ProductCategoryListAPIView(GeneralListApiView):
    serializer_class = ProductCategorySerializer

    # def get_queryset(self):
    #     return ProductCategory.objects.filter(state = True)