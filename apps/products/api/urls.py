from django.urls import path
from apps.products.api.views.general_views import MesaureUnitListAPIView, IndicatorListAPIView, ProductCategoryListAPIView


urlpatterns = [
    path("measure_unit/", MesaureUnitListAPIView.as_view(), name = 'measure_unit'),
    path("indicator/", IndicatorListAPIView.as_view(), name = 'indicator'),
    path("product_category/", ProductCategoryListAPIView.as_view(), name = 'product_category'),
]
