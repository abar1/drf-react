from rest_framework import serializers
from apps.products.models import MeasureUnit, ProductCategory, Indicator

class MesureUnitSerializer(serializers.ModelSerializer):

    class Meta:
        model = MeasureUnit
        exclude = ("state","created_on", "modified_on", "deleted_on")


class ProductCategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = ProductCategory
        exclude = ("state","created_on", "modified_on", "deleted_on")


class IndicatorSerializer(serializers.ModelSerializer):

    class Meta:
        model = Indicator
        exclude = ("state","created_on", "modified_on", "deleted_on")
