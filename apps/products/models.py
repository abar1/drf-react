from django.db import models
from apps.base.models import BaseModel
from simple_history.models import HistoricalRecords

class MeasureUnit(BaseModel):

    description = models.CharField("Descripción", max_length=50, blank=False, null=False)
    historical = HistoricalRecords()

    @property
    def _history_user(self):
        return self.changed_by

    @_history_user.setter
    def _history_user(self, value):
        self.changed_by = value

    class Meta:
        verbose_name = "Measure Unit"
        verbose_name_plural = "Measure Units"

    def __str__(self):
        return self.description


class ProductCategory(BaseModel):

    description = models.CharField("Descripción", max_length=50, unique=True, blank=False, null=False)
    historical = HistoricalRecords()

    @property
    def _history_user(self):
        return self.changed_by

    @_history_user.setter
    def _history_user(self, value):
        self.changed_by = value

    class Meta:
        verbose_name = "Product category"
        verbose_name_plural = "Product Categorys"

    def __str__(self):
        return self.description


class Indicator(BaseModel):

    discount_value = models.PositiveSmallIntegerField(default=0)
    category_product = models.ForeignKey(ProductCategory, verbose_name=("Indicador oferta"), on_delete=models.CASCADE)
    historical = HistoricalRecords()

    @property
    def _history_user(self):
        return self.changed_by

    @_history_user.setter
    def _history_user(self, value):
        self.changed_by = value

    class Meta:
        verbose_name = "Offer indicator"
        verbose_name_plural = "Offer indicators"

    def __str__(self):
        return f"{self.category_product}: {self.discount_value}%"


class Product(BaseModel):

    name = models.CharField("Nombre del producto", max_length=200, blank=False, null=False)
    description= models.TextField("Descripción del producto", blank=False, null=False)
    image = models.ImageField("Imagen del producto", upload_to="products/", blank=True, null=True)
    measure_unit = models.ForeignKey(MeasureUnit, verbose_name=("Unidad de medida"), on_delete=models.CASCADE, null=True)
    product_category = models.ForeignKey(ProductCategory, verbose_name=("Categoria producto"), on_delete=models.CASCADE, null=True)
    historical = HistoricalRecords()

    @property
    def _history_user(self):
        return self.changed_by

    @_history_user.setter
    def _history_user(self, value):
        self.changed_by = value

    class Meta:
        verbose_name = "Product"
        verbose_name_plural = "Products"

    def __str__(self):
        return self.name
