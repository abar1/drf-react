# Generated by Django 3.2.16 on 2023-01-20 17:40

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0004_auto_20230120_1728'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='historicalproductcategory',
            name='measure_unit',
        ),
        migrations.RemoveField(
            model_name='productcategory',
            name='measure_unit',
        ),
        migrations.AddField(
            model_name='historicalproduct',
            name='measure_unit',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='products.measureunit', verbose_name='Unidad de medida'),
        ),
        migrations.AddField(
            model_name='historicalproduct',
            name='product_category',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='products.productcategory', verbose_name='Categoria producto'),
        ),
        migrations.AddField(
            model_name='product',
            name='measure_unit',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='products.measureunit', verbose_name='Unidad de medida'),
        ),
        migrations.AddField(
            model_name='product',
            name='product_category',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='products.productcategory', verbose_name='Categoria producto'),
        ),
    ]
