from rest_framework import status
from apps.users.models import User
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import api_view
from apps.users.api.serializers import UserSerializer, UserListSerializer


@api_view(['GET', 'POST'])
def user_api_view(request):
    print(f"metodo {request.method}")
    if request.method == 'GET':
        users = User.objects.all().values('id', 'username', 'email', 'password')
        users_serializer = UserListSerializer(users, many = True) # con many true es cuando mandas un filter o all
        
        return Response(users_serializer.data, status = status.HTTP_200_OK)
    # elif request.method == 'POST':
    #     test_data = {
    #         "name": "bar bar",
    #         "email": "bar.bar@gmail.com"
    #     }
    #     users_serializer = UserListSerializer(data = test_data, context = test_data)
        
    #     if users_serializer.is_valid():
    #         user_instance = users_serializer.save()
    #         print(user_instance)
    #     else:
    #         print(users_serializer.errors)


    #     return Response(users_serializer.data, status = status.HTTP_200_OK)

    elif request.method == 'POST':
        users_serializer = UserSerializer(data = request.data)
        if users_serializer.is_valid():
            users_serializer.save()
            return Response(users_serializer.data, status = status.HTTP_201_CREATED)
        else:
            return Response(users_serializer.errors, status = status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def user_detail_api_view(request, pk=None):
    
    user = User.objects.filter(id=pk).first()

    if user:
        if request.method == 'GET':
            if pk is not None:
                user = User.objects.filter(id=pk).first()
                user_serializer = UserSerializer(user)
                return Response(user_serializer.data, status = status.HTTP_200_OK)

        elif request.method == 'PUT':
            # user_serializer = UserSerializer(user, data = request.data)
            user_serializer = UserSerializer(user, data = request.data)
            if user_serializer.is_valid():
                user_serializer.save()
                return Response(user_serializer.data, status = status.HTTP_201_CREATED)
            return Response(user_serializer.errors, status = status.HTTP_400_BAD_REQUEST)
        elif request.method == 'DELETE':
            user = User.objects.filter(id=pk).first()
            user.delete()
            return Response({"message": 'Usuario eliminado correctamente'})

    else:
        return Response({"meesage":"No se a encontrado un usuario con etos datos"}, status = status.HTTP_400_BAD_REQUEST)

# class UserAPIView(APIView):

#     def get(self, request):
#         users = User.objects.all()
#         users_serializer = UserSerializer(users, many = True) # con many true es cuando mandas un filter o all
#         return Response(users_serializer.data)