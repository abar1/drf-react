from django.urls import path
# from apps.users.api.api import UserAPIView
from apps.users.api.api import user_api_view, user_detail_api_view

urlpatterns = [
    path('users/', user_api_view, name = 'user_api_view'),
    path('users/<int:pk>/', user_detail_api_view, name ='user_detail_api_view'),
    # path('users/', UserAPIView.as_view(), name = 'users')
]
